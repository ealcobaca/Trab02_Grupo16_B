/*
 *
 * Trabalho 2 : Programacao Concorrente
 * Tema : Smooth em grandes imagens utilizando processamento paralelo
 * Alunos:
 * Edesio P. de Souza Alcobaca Neto - 8578872
 * Tales Prates Correia             - 8532151
*/



#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cstdio>
#include <exception>
#include <string>
#include <opencv2/opencv.hpp>
#include <stdlib.h>
#include <omp.h>
#include <mpi.h>
#include <sys/time.h>

#define SEND_SIZE 2*1024*1024 /* tamanho do conjunto de dados */

/* controle da entrada argv */
#define NAME_IN 1
#define NAME_OUT 2
#define TYPE 3
#define MASTER 0

struct timeval ini;
struct timeval fim;
double dif;


using namespace cv;

class Image{

	private:

		Mat img;
		int type;


		// Este metodo eh responsavel por processar o smooth para imagens em escala de cinza 
		// Basicamente realiza as o smooth para um dado conjunto de linha
		// Aqui processamos um conjunto de linhas que pode ser de tres tipos: linhas iniciais ( K linhas + radius ),
		// linhas intermediarias ( radius + K linhas + linhas), linhas finais ( K linhas + radius).
		// radius eh necessario para o calculo da k-esima linhas.
		//
		// Buff1 = conjunto de linhas originais, a qual precisaremos fazer o smoothing
		// este conjunto pode conter linhas que nao iremos processar o smooth, mas utiliza-la no calculo de
		// outras linha, deve-se setar jump para nao considera-las.
		// Buff2 = conjunto de linhas resultantes processadas com smooth, apenas linhas resultantes.
		// Jump = Caso 0, começa o processamento de smooth na primeira linha. Caso seja l > 0,
		// a o processamento parte da linha l.
		// radius = raio do smooth, para smooth com janela 5 o raio eh 2, com janela 9 o raio eh 4, etc ...
		// rows = quantas linahs de buff2 considerar para o smooth
		// cols = quantas colunas de buff1 considerar para o smooth
		// isFin = indica se o conjunto de linha a ser processado pertence ao fim da imagem, setar como 1.
		void processamento_gray(cv::Mat buff1, cv::Mat buff2, int radius, int rows, int cols, int jump, int isFin ){
			
			//Como o processamento do for pode ser paralelizado, dividiremos as K linhas em 4 threads
			#pragma omp parallel for firstprivate(buff1,buff2,rows,cols,jump,isFin,radius) num_threads(4)
			for(int i=jump;  i<rows+jump; i++){
				for( int j=0; j < cols; j++){
					unsigned int avg = 0;//variavel que armazena a soma dos pixeis no quadro
					unsigned int count = (2*radius+1)*(2*radius+1); //calculo do tamanho do quadro
					int k,l; // auxiliares para for

					for(k=i-radius; k <= i+radius; k++){//varrer todas as linhas
						for( l=j-radius; l <=j+radius; l++){//varrer todas as colunas

							//caso seja um pixel de borda e seus vizinhos nao possam ser acessados
							// decrementamos count, nao considerando na media
							
							if(isFin==1){//caso seja a ultima linha, nao temos linhas alem da ultima

								//verificar se esta fora da borda
								if(k < 0 || k >= rows+jump || l < 0 || l >= cols){
									count--;//pixel fora da matriz de imagem
									continue;
								}

							}else if(k < 0 ||  l < 0 || l >= cols){//caso o conjunto de linhas estaja no comeco ou fim
								count--;//pixel fora da matriz de imagem
								continue;
							}

							avg += buff1.at<uchar>(k, l);//acumulo dos valores

						}
					}

					buff2.at<uchar>(i-jump, j)= (unsigned char) (avg/count);//calculo da media

				}
			}

			return;



		}

		// Este metodo eh responsavel por processar o smooth para imagens rgb
		// Basicamente realiza as o smooth para um dado conjunto de linha
		// Aqui processamos um conjunto de linhas que pode ser de tres tipos: linhas iniciais ( K linhas + radius ),
		// linhas intermediarias ( radius + K linhas + linhas), linhas finais ( K linhas + radius).
		// radius eh necessario para o calculo da k-esima linhas.
		//
		// Buff1 = conjunto de linhas originais, a qual precisaremos fazer o smoothing
		// este conjunto pode conter linhas que nao iremos processar o smooth, mas utiliza-la no calculo de
		// outras linha, deve-se setar jump para nao considera-las.
		// Buff2 = conjunto de linhas resultantes processadas com smooth, apenas linhas resultantes.
		// Jump = Caso 0, começa o processamento de smooth na primeira linha. Caso seja l > 0,
		// a o processamento parte da linha l.
		// radius = raio do smooth, para smooth com janela 5 o raio eh 2, com janela 9 o raio eh 4, etc ...
		// rows = quantas linahs de buff2 considerar para o smooth
		// cols = quantas colunas de buff1 considerar para o smooth
		// isFin = indica se o conjunto de linha a ser processado pertence ao fim da imagem, setar como 1.

		void processamento_rgb(cv::Mat buff1, cv::Mat buff2, int radius, int rows, int cols, int jump, int isFin ){

			//Como o processamento do for pode ser paralelizado, dividiremos as K linhas em 4 threads
			#pragma omp parallel for firstprivate(buff1,buff2,rows,cols,jump,isFin,radius) num_threads(4)
			for(int i=jump;  i<rows+jump; i++){
				
				for( int j=0; j < cols; j++){
					
					//variaveis que armazena a soma dos pixeis no quadro
					unsigned int avgRed = 0;
					unsigned int avgGreen = 0;
					unsigned int avgBlue = 0;

					unsigned int count = (2*radius+1)*(2*radius+1);//calculo do tamanho do quadro
					int k,l;// auxiliares para for


					for(k=i-radius; k <= i+radius; k++){//varer todas as linhas
						for( l=j-radius; l <=j+radius; l++){//varrer todas as colunas
							
							//caso seja um pixel de borda e seus vizinhos nao possam ser acessados
							// decrementamos count, nao considerando na media

							if(isFin==1){//caso seja a ultima linha, nao temos linhas alem da ultima


								if(k < 0 || k >= rows+jump || l < 0 || l >= cols){
									count--;//fora da matriz de imagem
									continue;
								}

							}else if(k < 0 ||  l < 0 || l >= cols){//caso o conjunto de linhas estaja no comeco ou fim
								count--;//caso o pixel esteja fora da matriz da imagem
								continue;
							}

							//acumulo do valor do pixel
							avgRed += buff1.at<Vec3b>(k, l)[0];
							avgGreen += buff1.at<Vec3b>(k, l)[1];
							avgBlue += buff1.at<Vec3b>(k, l)[2];

						}
					}

					//calculo da media
					buff2.at<Vec3b>(i-jump, j)[0] = (unsigned char) (avgRed/count);
					buff2.at<Vec3b>(i-jump, j)[1] = (unsigned char) (avgGreen/count);
					buff2.at<Vec3b>(i-jump, j)[2] = (unsigned char) (avgBlue/count);

				}
			}

			return;

		}


		// Metodo para calcular quantidade de linhas correspondente a X bytes.	
		// selecionar n quantidades de linhas tais que tenham tamanho X, onde X <= size (em termos de bytes)
		//
		// cols = numero de colunas na imagem
		// nChannel = quantidade de cais (rgb=3 ==> 3 bytes)
		// size = tamnho maximo de bytes do conjunto de linhas
		// return < retorna o numero de linhas menor ou igua a size.
		int numRows(int cols, int size, int nChannel){

			int sizeLim = cols*nChannel;
			if(sizeLim > size) return 1;//linha ter mais que sizeMByte
			else return size/sizeLim;	
		}


		// Produtor e Receptor de trabalhos realizados pelas threads
		// Este metodo manda os blocos de K linhas para serem processados pelos nohs,
		// e recebe os blocos processados com o resultado
		//
		// smoothed = variavel que recebera a imagem com o smooth
		// image = imagem que passaremos o filtro smooth
		// sizeBlock = total de blocos que a imagem foi quebrada
		// sizeProc = total de processos ativos para computacao 
		// sizeLine = quantidade de de bytes em uma linha da imagem
		// nRows = quantidade de linhas por bloco
		
		void prodCons(Mat smoothed, Mat image, int radius, int sizeBlock, int sizeProc, int sizeLine, int nRows){
			int ped[sizeProc]; // vetor para armazenar qual bloco foi enviado para um processo  indicado pelo indice do vetor
			int cols = image.cols; // quantidade de colunas da imagem
			int rows = image.rows; // quantidade de linhas da imagem

			for (int block = 0; block < sizeBlock; ){ //enquanto todos os blocks nao forem enviados
				int k=0;
				int rank_id;//rank do processo

				// enviar trabalho para todos os processos disponiveis 
				for( rank_id=1,k=1; k < sizeProc && block < sizeBlock; k++,rank_id++){
					
					//sinalizar que ha um trabalho a ser realizado e qual o bloco:
					MPI_Send( &block, 1, MPI_INT, rank_id, 1, MPI_COMM_WORLD);
					ped[k] = block;//armazena o bloco que foi enviado

					//linha corrente que sera enviada
					int curLine = (block) * nRows;

					if(block == 0){ // caso seja o primeiro bloco
						// envia as nRows primeiras linhas, e mais 2, para o calculo da ultima linha do bloco
						MPI_Send(&(image.data[curLine*sizeLine]), sizeLine * (nRows+radius),
								MPI_UNSIGNED_CHAR, rank_id, 1, MPI_COMM_WORLD);

					}else if( block == sizeBlock -1 ){ // caso seja a ultima linha
						
						// envia as ultimas linhas, e mais 2 anteriores , para o calculo da borda do bloco
						if( rows%nRows == 0 ) // caso tenha exatamente nRows linhas 
							MPI_Send(&(image.data[(curLine-radius)*sizeLine]), (sizeLine) * (nRows+radius),
									MPI_UNSIGNED_CHAR, rank_id, 1, MPI_COMM_WORLD);
						else // caso nao tenha nRows linhas
							MPI_Send(&(image.data[(curLine-radius)*sizeLine]), (sizeLine) * ((rows%nRows)+radius),
									MPI_UNSIGNED_CHAR, rank_id, 1, MPI_COMM_WORLD);
					}
					else{ // caso seja uma linha intermediaria

						// envia nRows linhas , 2 linhas anteriores ao comeco do bloco e 2 posteriores ao final do bloco
						// isto para calcular o smooth da borda do bloco.
						MPI_Send(&(image.data[(curLine-radius)*sizeLine]), (sizeLine) * (nRows+radius+radius),
								MPI_UNSIGNED_CHAR, rank_id, 1, MPI_COMM_WORLD);
					}

					block++;
				}

				// recebe o trabalho de todos os processos que receberam uma tarefa 
				for(int rank_id=1, j=1; j < k; j++,rank_id++){

					int block_aux = ped[j]; //bloco que sera recebido
					int curLine = block_aux * nRows; // linha corrente do inicio do bloco

					if(block_aux == 0){// caso seja o primeiro bloco
						// recebe as nRows primeiras linhas processadas
						MPI_Recv(&(smoothed.data[curLine*sizeLine]), sizeLine * nRows,
								MPI_UNSIGNED_CHAR, rank_id, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

					}else if( block_aux == sizeBlock -1 ){ // caso seja o ultimo bloco

						// recebe o ultimo bloco (ultimas linhas) processadas.
						if( rows%nRows == 0 ) // caso tenha exatamente nRows linhas 
							MPI_Recv(&(smoothed.data[curLine*sizeLine]), sizeLine * nRows,
									MPI_UNSIGNED_CHAR, rank_id, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
						else // caso o bloco nao tenha nRows 
							MPI_Recv(&(smoothed.data[curLine*sizeLine]), sizeLine * (rows%nRows),
									MPI_UNSIGNED_CHAR, rank_id, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
					}
					else{ //caso seja uma linha intermediaria

						//recebe blocos intermediarios
						MPI_Recv(&(smoothed.data[curLine*sizeLine]), sizeLine * nRows,
								MPI_UNSIGNED_CHAR, rank_id, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
					}

				}



			}


			int job = -1;
			//finalizar comunicacao
			for(int i=1; i<sizeProc; i++ ){
				MPI_Send( &job, 1, MPI_INT, i, 1, MPI_COMM_WORLD);
			}


		}

		//
		void slaveJob(int cols, int rows, int nRows, int nChannel, int radius, int sizeBlock, int tp_image){


			cv::Mat buff1(nRows+(2*radius), cols, tp_image); // buff que recebe bloco para tarefa 
			cv::Mat buff2(nRows, cols, tp_image); // buff para o resultado da tarefa
			
			int sizeLine = cols * nChannel; // numeros de bytes que uma linha tem
			int block; // numero do bloco

			while(1==1){ //enquanto tem trabalhos para ser realizado

				//verificar se ha trabalho
				MPI_Recv( &block, 1, MPI_INT, 0,1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

				if(block == -1) break; //caso nao haja mais trabalhos

				if(block == 0){// o bloco recebido foi o com linhas iniciais

					//Recebe o bloco
					MPI_Recv(&(buff1.data[0]), sizeLine * (nRows+radius), MPI_UNSIGNED_CHAR, 
						MASTER, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

					//dependendo da imagem (rgb, gray), faz a computacao
					if(nChannel == 3) processamento_rgb(buff1, buff2, radius, nRows, cols, 0,0);
					else if(nChannel == 1) processamento_gray(buff1, buff2, radius, nRows, cols, 0,0);

					// feito o processamento envia o resultado
					MPI_Send(&(buff2.data[0]), sizeLine*nRows, MPI_UNSIGNED_CHAR, MASTER, 1, MPI_COMM_WORLD);


				}else if( block == sizeBlock -1 ){ // bloco recebido eh o com as linahs finais

					if( rows%nRows == 0 ){ // caso o bloco seja completo, isto eh, tenha nRows linhas

						//recebe blocos
						MPI_Recv(&(buff1.data[0]), sizeLine * (nRows+radius),MPI_UNSIGNED_CHAR, 
							MASTER, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

						// dependendo da imagem (rgb, gray), faz a computacao
						if(nChannel == 3) processamento_rgb(buff1, buff2, radius, nRows, cols, radius,1);
						else if(nChannel == 1)  processamento_gray(buff1, buff2, radius, nRows, cols, radius,1);

						// feito o processamento envia o resultado
						MPI_Send(&(buff2.data[0]), sizeLine*nRows, MPI_UNSIGNED_CHAR, MASTER, 1, MPI_COMM_WORLD);

					}
					else{ // caso o bloco final nao seja completo, < nRows

						//recebe blocos
						MPI_Recv(&(buff1.data[0]), sizeLine * ((rows%nRows)+radius),
								MPI_UNSIGNED_CHAR, MASTER, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

						//dependendo da imagem (rgb, gray), faz a computacao
						if(nChannel == 3)
							processamento_rgb(buff1, buff2, radius, rows%nRows, cols, radius,1);
						else if(nChannel == 1)
							processamento_gray(buff1, buff2, radius, rows%nRows, cols, radius,1);

						//feito o processamento envia o resultado
						MPI_Send(&(buff2.data[0]), sizeLine*(rows%nRows), MPI_UNSIGNED_CHAR, MASTER, 1, MPI_COMM_WORLD);

					}
				}
				else{ // caso o bloco final seja um intermediario

					//recebe o bloco
					MPI_Recv(&(buff1.data[0]), sizeLine * (nRows+radius+radius),
							MPI_UNSIGNED_CHAR, MASTER, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

					// dependendo da imagem (rgb, gray), faz a computacao
					if(nChannel == 3)
						processamento_rgb(buff1, buff2, radius, nRows, cols, radius,0);
					else if(nChannel == 1)
						processamento_gray(buff1, buff2, radius, nRows, cols, radius,0);

					// feito o processamento envia o resultado
					MPI_Send(&(buff2.data[0]), sizeLine*nRows, MPI_UNSIGNED_CHAR, MASTER, 1, MPI_COMM_WORLD);

				}

			}

			return;

		}

	public:

		void smooth(int radius, const char *name){

			int sizeProc,rank;
			int cols; //numero de colunas
			int rows; //numero de linhas
			int nRows; //numero de linhas em um bloco
			int nChannel; //numero de canais
			int tp_image; //tipo da imagem (jpg, ppm, ...)
			int sizeBlock; //quantidade de linahs que um bloco tem
			int sizeLine; //quantidade em bytes que uma linha da imagem possui
			int provided;
			cv::Mat image;

			//habilita seguranca no processamento das funcoues MPI com thread
			MPI_Init_thread( NULL, NULL, MPI_THREAD_MULTIPLE, &provided );
			MPI_Comm_rank(MPI_COMM_WORLD,&rank);//pega o numero do rank do processo
			MPI_Comm_size(MPI_COMM_WORLD,&sizeProc);//pega a quantidade de processos

			//char processor_name[MPI_MAX_PROCESSOR_NAME];
			//int name_len;
			//MPI_Get_processor_name(processor_name, &name_len);

			//printf("Hello world from processor %s, rank %d out of %d processors numthr= %d\n",
			//processor_name, rank, sizeProc,omp_get_max_threads());


			if(rank == 0){//mestre  

				//verifica o tipo da imagem e quantos canais ela tem	
				if(this->type == 0)
					nChannel = 1;
				else
					nChannel = 3;

				//toma o numero de linhas e colunas da imagem
				image = this->img;
				rows = image.rows;
				cols = image.cols;

				// calcula o nro de linhas que deve ter para
				// que um bloco tenha no maximo SEND_SIZE bytes

				if (SEND_SIZE < cols*rows*nChannel){ // caso a linha seja menor que SEND_SIZE

					nRows =numRows(cols, SEND_SIZE, 3);
					sizeBlock = (int)ceil(rows/(nRows*1.0));
				}else{ // caso a linhas seja maior que SEND_SIZE

					nRows = rows;
					sizeBlock = 1;
				}
				
				//verifica o tipo da imagem
				tp_image = image.type();

			}

			//envia todos os dados necessarios para a execucao das tasks
			//via broadcast para os processos
			MPI_Bcast(&nChannel, 1, MPI_INT, MASTER, MPI_COMM_WORLD);
			MPI_Bcast(&tp_image, 1, MPI_INT, MASTER, MPI_COMM_WORLD);
			MPI_Bcast(&sizeBlock, 1, MPI_INT, MASTER, MPI_COMM_WORLD);
			MPI_Bcast(&nRows, 1, MPI_INT, MASTER, MPI_COMM_WORLD);
			MPI_Bcast(&rows, 1, MPI_INT, MASTER, MPI_COMM_WORLD);
			MPI_Bcast(&cols, 1, MPI_INT, MASTER, MPI_COMM_WORLD);


			if(rank == 0){ // caso seja o mestre

				//cria variavel para a imagem smoothed
				Mat smoothed(rows, cols, tp_image);
				sizeLine = cols*nChannel;

				//distribuir os jobs para processa o smooth
				prodCons(smoothed, image, radius, sizeBlock, sizeProc, sizeLine, nRows);

				//escreve imagem resultante
				gettimeofday( &fim, NULL );	
				cv::imwrite(name, smoothed);

				dif = fim.tv_sec - ini.tv_sec;/*diferenca entre segundos*/
				dif += (fim.tv_usec - ini.tv_usec)/1e6; /*somar microsegundos*/	
				printf("%lf\n",dif);


			}else{ // demais nos ou escravos

				//tarefa dos nohs
				slaveJob(cols, rows, nRows, nChannel, radius, sizeBlock,tp_image);

			}


			MPI_Finalize();
			return;

		}


		//le imagem de um arquivo
		void readImage(const char* filename){

			this->img = imread(filename,this->type);
		}

		//escreve imagem em um arquvo
		void writeImage(const char* filename){
			imwrite(filename, this->img);
		}

		//getters e setters
		void setType(int type){ this->type = type; }
		int getType(){ return this->type; }

		Image(){
			this->type = 0;
		}
		virtual ~Image(){
			this->img.release();
		}
};




int main(int argc, char *argv[]){
	Image image;


	if(argc != 4){//entrada inesperada
		
		std::cout << "Usage: [input name].[extension] [output name].[extension] [rgb/gray]" << std::endl;
	}else{

		//verifica o tipo da imagem
		if(strcmp(argv[TYPE],"rgb") == 0 ) image.setType(1);
		else if(strcmp(argv[TYPE],"gray") == 0) image.setType(0);
		else{
			std::cout << "please use rgb or gray." << std::endl;
			return 0;
		}

		//le imagem do hd
		image.readImage(argv[NAME_IN]);

		gettimeofday( &ini, NULL );/*tempo eh pego em segundos*/
		image.smooth(2,argv[NAME_OUT]); //processa o smooth
	}

	return 0;
}


/*
 *
 * Trabalho 2 : Programacao Concorrente
 * Tema : Smooth em grandes imagens utilizando processamento paralelo
 * Alunos:
 * Edesio P. de Souza Alcobaca Neto - 8578872
 * Tales Prates Correia             - 8532151
*/

#include <cstdio>
#include <iostream>
#include <string.h>
#include <exception>
#include <opencv2/opencv.hpp>
#include <sys/time.h>

/* controle da entrada argv */
#define NAME_IN 1
#define NAME_OUT 2
#define TYPE 3

/* valores para calulo do tempo */
struct timeval ini;
struct timeval fim;
double dif;

using namespace cv;

class Image{

	private:
		Mat image; //objeto que contem a imagem
		int type; //tipo onde 1 = rgb e 0 = gray


	// Este metodo processa o smooth de this->image, para imagem em gray
	//
	// radius = raio de alcance do smooth, isto eh, para uma janela 5x5 o raio eh 2, para uma janela 9x9 o raio eh 4 e etc...
	void smooth_gray(int radius){

			int rows = this->image.rows;//numero de linhas da imagem
			int cols = this->image.cols;//numero de colunas da imagem

			Mat smoothed(rows, cols, this->image.type()); // cria buffer para imagem smoothed

			for(int i=0;i<this->image.rows;i++)//percorre todas as linas
				for(int j=0;j<this->image.cols;j++)// e percorre todas as colunas na imagem
				{
					unsigned int avgGray = 0;//variavel que acumula a soma dos pixeis na janela
					unsigned int count = (2*radius+1)*(2*radius+1);//tamanho da janela

					for(int k=i-radius;k <= i+radius;k++) //para todas as linhas da janela
						for(int l=j-radius;l <=j+radius;l++){ //para cada coluna da janela
							
							//caso o pixel esteja fora das bordas da imagem
							if(k < 0 || k >= this->image.rows || l < 0 || l >= this->image.cols){
								count--;//nao leva em consideracao na media
								continue;
							}else{
								avgGray += this->image.at<uchar>(k, l);//acumula valor do pixel
							}
						}

					//faz media
					smoothed.at<uchar>(i, j) = (unsigned char) (avgGray/count);

				}

			this->image.release();
			this->image = smoothed;
		}


		// Este metodo processa o smooth de this->image, para imagens em rgb
		//
		// radius = raio de alcance do smooth, isto eh, para uma janela 5x5 o raio eh 2, para uma janela 9x9 o raio eh 4 e etc...
		void smooth_rgb(int radius){

			int rows = this->image.rows;//numero de linhas 
			int cols = this->image.cols;//numero de colunas

			//buffer para imagem smoothed
			Mat smoothed(rows, cols, this->image.type());

			for(int i=0;i<this->image.rows;i++) // para todas linhas da imagem
				for(int j=0;j<this->image.cols;j++){ // e para toda as colunas
				
					//variavel que acumula a soma dos pixeis
					unsigned int avgRed = 0;
					unsigned int avgGreen = 0;
					unsigned int avgBlue = 0;

					//tamanho da janela
					unsigned int count = (2*radius+1)*(2*radius+1);

					for(int k=i-radius;k <= i+radius;k++)//para todas as linahs da janela
						for(int l=j-radius;l <=j+radius;l++){//para todas as colunas da janela
						
							//caso o pixel esteja fora das bordas da imagem
							if(k < 0 || k >= this->image.rows || l < 0 || l >= this->image.cols){
								count--;//nao levar em consideracao na media das imagens
								continue;
							}else{
								//acumula valor dos pixeis
								avgRed += this->image.at<Vec3b>(k, l)[0];
								avgGreen += this->image.at<Vec3b>(k, l)[1];
								avgBlue += this->image.at<Vec3b>(k, l)[2];
							}
						}

					//calcula a media
					smoothed.at<Vec3b>(i, j)[0] = (unsigned char) (avgRed/count);
					smoothed.at<Vec3b>(i, j)[1] = (unsigned char) (avgGreen/count);
					smoothed.at<Vec3b>(i, j)[2] = (unsigned char) (avgBlue/count);

				}

			this->image.release();
			this->image = smoothed;
		}

	public:

		void smooth(int radius){
			if(this->type == 0)//escala de cinza
				this->smooth_gray(radius);
			else if(this->type == 1)
				this->smooth_rgb(radius);
		}

		void readImage(const char* filename){

			this->image = imread(filename,this->type);
		}


		void writeImage(const char* filename){
			imwrite(filename, this->image);
		}

		void setType(int type){ this->type = type; }
		int getType(){ return this->type; }

		Image(){
			this->type = 0;
		}
		virtual ~Image(){
			this->image.release();
		}
};



int main(int argc, char *argv[]){
	Image image;


	if(argc != 4){ // caso esteja fora dos padroes

		std::cout << "Usage: [input name].[extension] [output name].[extension] [rgb/gray]" << std::endl;
	}else{

		int type;
		if(strcmp(argv[TYPE],"rgb") == 0 ) image.setType(1);
		else if(strcmp(argv[TYPE],"gray") == 0) image.setType(0); 
		else{
			std::cout << "please use rgb or gray." << std::endl;
			return 0;
		}

		//ler imagem do hd
		image.readImage(argv[NAME_IN]);
		
		gettimeofday( &ini, NULL );/*tempo eh pego em segundos*/
		image.smooth(2);//calcular smooth
		gettimeofday( &fim, NULL );
		

		dif = fim.tv_sec - ini.tv_sec;/*diferenca entre segundos*/
		dif += (fim.tv_usec - ini.tv_usec)/1e6; /*somar microsegundos*/
		printf("%lf\n",dif);

		//escrever imagem
		image.writeImage(argv[NAME_OUT]);

	}

	return 0;
}

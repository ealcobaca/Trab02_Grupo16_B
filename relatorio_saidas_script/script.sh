#/bin/bash
make compile
for core in 12 14 16 17; do #laco para repetir com np igual a (1 3 5 7 9)
	for counter in {1..10..1}; do #laco para colher 5 amostras
		echo 'Execucao('$counter') CORE='$core' THREAD=4' >> finalCountdown/saidaNP.txt
		make core=$core test >> finalCountdown/saidaNP.txt
	done
#done

perl -pi -e 's/num_threads\(4\)/num_threads\(1)/g' code/paralelo.cc #substitui 4 threads para 1 threads por processo
make compile
for counter in {1..10..1}; do
	echo 'Execucao('$counter') CORE=16 THREAD=1 SEND_SIZE=512K' >> finalCountdown/saidaTHREAD01.txt
	make core=16 test >> finalCountdown/saidaTHREAD01.txt
done

perl -pi -e 's/num_threads\(1\)/num_threads\(2)/g' code/paralelo.cc #substitui 1 thread para 2 threads por processo
make compile
for counter in {1..10..1}; do
	echo 'Execucao('$counter') CORE=16 THREAD=2 SEND_SIZE=512K' >> finalCountdown/saidaTHREAD02.txt
	make core=16 test >> finalCountdown/saidaTHREAD02.txt
done

perl -pi -e 's/num_threads\(2\)/num_threads\(8)/g' code/paralelo.cc #substitui 2 threads para 8 threads por processo
make compile
for counter in {1..10..1}; do
	echo 'Execucao('$counter') CORE=16 THREAD=8 SEND_SIZE=512K' >> finalCountdown/saidaTHREAD08.txt
	make core=16 test >> finalCountdown/saidaTHREAD08.txt
done

perl -pi -e 's/num_threads\(8\)/num_threads\(16)/g' code/paralelo.cc #substitui 8 threads pro 16 threads pro processo
make compile
for counter in {1..10..1}; do
	echo 'Execucao('$counter') CORE=16 THREAD=16 SEND_SIZE=512K' >> finalCountdown/saidaTHREAD16.txt
	make core=16 test >> finalCountdown/saidaTHREAD16.txt
done

perl -pi -e 's/num_threads\(16\)/num_threads\(4)/g' code/paralelo.cc #substitui 16 threads para o padrao de 4 threads por processo

perl -pi -e 's/2\*1024\*1024/512\*1024/g' code/paralelo.cc #substitui 16 threads para o padrao de 4 threads por processo
make compile
for counter in {1..10..1}; do
	echo 'Execucao('$counter') CORE=16 THREAD=4 SEND_SIZE=512K' >> finalCountdown/saidaSENDSIZE0512.txt
	make core=16 test >> finalCountdown/saidaSENDSIZE0512.txt.txt
done

perl -pi -e 's/512\*1024/2\*1024\*1024/g' code/paralelo.cc #substitui 16 threads para o padrao de 4 threads por processo
make compile
for counter in {1..10..1}; do
	echo 'Execucao('$counter') CORE=16 THREAD=4 SEND_SIZE=2MB' >> finalCountdown/saidaSENDSIZE2048.txt.txt
	make core=16 test >> finalCountdown/saidaSENDSIZE2048.txt.txt
done

perl -pi -e 's/2\*1024\*1024/5\*1024\*1024/g' code/paralelo.cc #substitui 16 threads para o padrao de 4 threads por processo
make compile
for counter in {1..10..1}; do
	echo 'Execucao('$counter') CORE=16 THREAD=4 SEND_SIZE=5MB' >> finalCountdown/saidaSENDSIZE5120.txt.txt
	make core=16 test >> finalCountdown/saidaSENDSIZE5120.txt.txt
done

perl -pi -e 's/5\*1024\*1024/2\*1024\*1024/g' code/paralelo.cc #substitui 16 threads para o padrao de 4 threads por processo

exit 0

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <math.h>
#include <omp.h>

#define SEND_SIZE 5 //quantidade de lihas
#define INTER 2 //intervalo
 
int getProc(int *aval, int *ult, int size, int set){
	int i;
	
	//printf("ult=%d, size=%d, set=%d\n",*ult,size,set);
	if(*ult >= size) *ult = 0;
	for(i=(*ult); i<size; i++)
		if(aval[i] == set ){ *ult=i; return i;}

	*ult = 0;
	return -1; 
}



int main(int argc,char ** argv) {

	int sizeProc,rank;
	int i,j,source = 0;
	int rank_id,job;
	int pedaco = 0, tot_pedaco;
	int provided;
	int togpi[5];
	//MPI_Init(&argc,&argv);
	MPI_Init_thread( &argc, &argv, MPI_THREAD_MULTIPLE, &provided )	;
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);
	MPI_Comm_size(MPI_COMM_WORLD,&sizeProc);
	

	tot_pedaco = SEND_SIZE;


	if(rank == 0){//mestre	

		int *aval = (int *) calloc(sizeof(int),sizeProc);
		aval[0] = -1;
		int ult = 0;
		int pedaco_aux;

		printf("%d\n",omp_get_max_threads());
		printf("%d\n",tot_pedaco);
		job =1;
		#pragma omp parallel private(pedaco,rank_id,pedaco_aux) firstprivate(tot_pedaco,ult,sizeProc,job) shared(aval)
		{
			#pragma omp sections nowait
			{
			//#pragma omp for
			#pragma omp section
				for(pedaco = 0; pedaco < tot_pedaco; pedaco++){//thread responsavel por enviar dados
					//#pragma omp critical (cri1)
					while( (rank_id = getProc(aval, &ult,sizeProc,0)) == -1);
					/*	
					while(rank_id == -1){
			
					//	#pragma omp critical
						rank_id = getProc(aval, &ult, sizeProc, 0);
					}*/
				

					//int pedaco_aux = pedaco;
					//int nt = omp_get_thread_num();
					//printf("[th %d] pedaco = %d\n",nt,pedaco);

					
					//sinalizar que ha um trabalho a ser realizado
					MPI_Send( &job, 1, MPI_INT, rank_id, 1, MPI_COMM_WORLD);

					//enviar matriz
					MPI_Send( &pedaco, 1, MPI_INT, rank_id, 1, MPI_COMM_WORLD);
					printf("[mestre] mandei o pedaco %d, para o %d\n",pedaco,rank_id);
				
					#pragma omp critical
					aval[rank_id] = 1;
	
				}

			
			//#pragma omp for 
			#pragma omp section
				for(pedaco = 0; pedaco < tot_pedaco; pedaco++){//thread responsavel por receber resultados
					
					while( (rank_id = getProc(aval, &ult,sizeProc,1)) == -1);
					
					//enviar matriz
					MPI_Recv( &pedaco_aux, 1, MPI_INT, rank_id, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
					printf("[mestre] recebi o pedaco %d, do %d\n",pedaco_aux,rank_id);
					togpi[(-1*pedaco_aux)] = pedaco_aux;
					
					#pragma omp critical
					aval[rank_id] = 0;
					
				}
		

			}


		}
		for(int i=0; i<5; i++){
			printf("%d \n",togpi[i]);
		}

		job = 0;
		//finalizar comunicacao
		for(int i=1; i<sizeProc; i++ ){

			MPI_Send( &job, 1, MPI_INT, i, 1, MPI_COMM_WORLD);
		}

		printf("[Mestre] Trabalho finalizado %d\n",tot_pedaco);

	}else{ //escravos


		while(1==1){//enquanto tem trabalhos para ser realizado

			//verificar se tem trabalho
			MPI_Recv( &job, 1, MPI_INT, 0,1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			if(job != 1) break;

			//pegar dados para realizar computação.
			MPI_Recv( &pedaco, 1, MPI_INT, 0,1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			printf("[Processo %d] Recebi pedaco %d\n",rank,pedaco);

			//faz computacao e envia resultados
			pedaco = -pedaco; 
			MPI_Send( &pedaco, 1, MPI_INT, 0, 1, MPI_COMM_WORLD);

		}

		printf("[Processo %d] Finalizado\n",rank);
	}


	MPI_Finalize();
	return 0;
}

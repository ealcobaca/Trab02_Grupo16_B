#include <cstdio>
#include <exception>
#include <string>
#include <opencv2/opencv.hpp>
#include <stdlib.h>
#include <omp.h>
#include <mpi.h>


using namespace cv;

#define MASTER 0
#define SLAVE 1

int main(int argc, char *argv[]){
	Mat image;

	int rank;
	int size;
	/* SEQUENTIAL CODE
	* image.readImage("Scorpion.jpg");
	* image.smooth(2);
	* image.writeImage("Scorpion_Smoothed.jpg");
	*/


	MPI_Init (&argc, &argv);
	MPI_Comm_rank (MPI_COMM_WORLD, &rank);       
	MPI_Comm_size (MPI_COMM_WORLD, &size);      


	if(rank == MASTER){


		//enviar pedacos da img
		
		image = imread("Scorpion.jpg");
		std::cout << image.type() << "\n";

		
		MPI_Send(&(image.rows), 1, MPI_INT, SLAVE, 0, MPI_COMM_WORLD);
		MPI_Send(&(image.cols), 1, MPI_INT, SLAVE, 1, MPI_COMM_WORLD);
		//MPI_Send(&(image.data[0]), image.rows*image.cols*3, MPI_UNSIGNED_CHAR, SLAVE, 2, MPI_COMM_WORLD);
		for(int i =0; i< image.rows; i++){
			//MPI_Send(&i, 1, MPI_INT, SLAVE, 2, MPI_COMM_WORLD);
			MPI_Send(&(image.data[i*image.cols*3]), image.cols*3, MPI_UNSIGNED_CHAR, SLAVE, 2, MPI_COMM_WORLD);
		}


	} else { //else == SLAVE
		
		int rows, cols;
		
		MPI_Recv(&rows, 1, MPI_INT, MASTER, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);	
		MPI_Recv(&cols, 1, MPI_INT, MASTER, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);	

		Mat smoothed(rows, cols, 16);
		//MPI_Recv(&(smoothed.data[0]), rows*cols*3, MPI_UNSIGNED_CHAR, MASTER, 2, MPI_COMM_WORLD, MPI_STATUS_IGNORE);	
		//void *smoothed = (void*)malloc(rows*cols*3);
		int i;
		int pos;		
		for(i=0; i<rows; i++){
		//	printf("readall\n");
			//MPI_Recv(&pos, cols*3, MPI_UNSIGNED_CHAR, MASTER, 2, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			MPI_Recv(&(smoothed.data[i*cols*3]), cols*3, MPI_UNSIGNED_CHAR, MASTER, 2, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			//MPI_Recv(&(smoothed.data[i*image.cols*3]), cols*3, MPI_UNSIGNED_CHAR, MASTER, 2, MPI_COMM_WORLD, MPI_STATUS_IGNORE);	
			//printf("%d\n",pos);
		}

		imwrite("Scorpion_Smoothed.jpg", smoothed);
	
	}

	MPI_Finalize();

	return 0;
}

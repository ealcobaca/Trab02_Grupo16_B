cmake_minimum_required(VERSION 2.8)
project( paralelo )
find_package( OpenCV REQUIRED )
#add_executable( paralelo paralelo.cc )
#target_link_libraries( paralelo ${OpenCV_LIBS} )
find_package(OpenMP)
if (OPENMP_FOUND)
    set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
    set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
endif()	
find_package(MPI REQUIRED)

include_directories(${MPI_INCLUDE_PATH})

add_executable(paralelo paralelo.cc)
target_link_libraries(paralelo ${MPI_LIBRARIES} ${OpenCV_LIBS})

if(MPI_COMPILE_FLAGS)
  set_target_properties(paralelo PROPERTIES
    COMPILE_FLAGS "${MPI_COMPILE_FLAGS}")
endif()

if(MPI_LINK_FLAGS)
  set_target_properties(paralelo PROPERTIES
    LINK_FLAGS "${MPI_LINK_FLAGS}")
endif()

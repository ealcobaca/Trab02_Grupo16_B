#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cstdio>
#include <exception>
#include <string>
#include <opencv2/opencv.hpp>
#include <stdlib.h>
#include <omp.h>
#include <mpi.h>

#define SEND_SIZE 1024*512 //quantidade de lihas
#define MASTER 0
#define RANGE 2

using namespace cv;

void processamento(cv::Mat buff1, cv::Mat buff2, int rows, int cols, int jump, int id ){

	//int i=jump,j=0;
	//printf("%d %d %d\n",rows,cols,jump);

	#pragma omp parallel for firstprivate(buff1,buff2,rows,cols,jump,id)
	for(int i=jump;  i<rows+jump; i++){
		for( int j=0; j < cols; j++){

			unsigned int avgRed = 0;
			unsigned int avgGreen = 0;
			unsigned int avgBlue = 0;
		
			unsigned int count = (2*RANGE+1)*(2*RANGE+1);
			int k,l;
			for(k=i-RANGE; k <= i+RANGE; k++){
				for( l=j-RANGE; l <=j+RANGE; l++){
                       			
					//std::cout << "(" << k << ", " << l << ")";
					if(id==1){//eh a ultima linha

						if(k < 0 || k >= rows+jump || l < 0 || l >= cols){
								 count--;
                                                                continue;
						}

					}else if(k < 0 ||  l < 0 || l >= cols){
                                                                count--;
                                                                continue;
					}

					avgRed += buff1.at<Vec3b>(k, l)[0];
					avgGreen += buff1.at<Vec3b>(k, l)[1];
					avgBlue += buff1.at<Vec3b>(k, l)[2];
					
				}
			}

			//printf("%d %d \n",k,l); return;
					//std::cout << count << "\n";
			//if(jump>0)
			//printf("[%d , %d]conunt %d\n",i,j,count);	
				//if(count == 0){ std::cout << "(" << i << ", " << j << ")";return;}
			buff2.at<Vec3b>(i-jump, j)[0] = (unsigned char) (avgRed/count);
			buff2.at<Vec3b>(i-jump, j)[1] = (unsigned char) (avgGreen/count);
			buff2.at<Vec3b>(i-jump, j)[2] = (unsigned char) (avgBlue/count);

		}
		//printf("thread = %d lin=%d \n", omp_get_thread_num(),i);
	}

	//printf("i_ini = %d  i_fim=%d \n",jump, i-1);

	return;

}

int getProc(int *aval, int *ult, int size, int set){
	int i;
	
	//printf("ult=%d, size=%d, set=%d\n",*ult,size,set);
	if(*ult >= size) *ult = 0;

	//printf("%d\n",set);
	
	if(set == -2){

		for(i=(*ult); i<size; i++){
			if( aval[i] == set ){ *ult=i; return i; }
		}

	}else if(set == 1){

		for(i=(*ult); i<size; i++){
			if( aval[i] >= 0 ){ *ult=i; return i; }
		}

	}
	*ult = 0;
	return -1; 
}

//selecionar x quantidades de linhas que tenham tamanho X ('size')
int numRows(int cols, int size, int nChannel){

	int sizeLim = cols*nChannel;
	//printf("%d %d",sizeLim, size);
	if(sizeLim > size) return 1;//linha ter mais que sizeMByte
	else return size/sizeLim;	
}


int main(int argc,char ** argv) {

	int sizeProc,rank;
	int cols, rows,nRows;
	int pedaco, tot_pedaco;
	int sizeLine;
	int provided;

	cv::Mat image;

	MPI_Init_thread( &argc, &argv, MPI_THREAD_MULTIPLE, &provided )	;
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);
	MPI_Comm_size(MPI_COMM_WORLD,&sizeProc);


	if(rank == 0){//mestre	

		int nChannel = 3;

		image = cv::imread("Scorpion.jpg");
		rows = image.rows;
		cols = image.cols;
	
		if (SEND_SIZE < cols*rows*nChannel){

			nRows =numRows(cols, SEND_SIZE, 3);
			tot_pedaco = (int)ceil(rows/(nRows*1.0));
		}else{

			nRows = rows;
			tot_pedaco = 1;
		}
		printf("nr=%d tot=%d rest=%d\n" ,nRows, tot_pedaco, rows%nRows);

	}

	MPI_Bcast(&tot_pedaco, 1, MPI_INT, MASTER, MPI_COMM_WORLD);
	MPI_Bcast(&nRows, 1, MPI_INT, MASTER, MPI_COMM_WORLD);
	MPI_Bcast(&rows, 1, MPI_INT, MASTER, MPI_COMM_WORLD);
	MPI_Bcast(&cols, 1, MPI_INT, MASTER, MPI_COMM_WORLD);

	
	if(rank == 0){

		//criando lista de processos disponiveis
		int *aval = (int *) calloc(sizeof(int),sizeProc);
		for(int i =0; i<sizeProc; i++) aval[i] = -2;
		aval[0] = -1;

                cv::Mat smoothed(rows, cols, 16); //imagem com resultado
		sizeLine = cols*3;

		//printf("%d\n",omp_get_max_threads());
		//printf("%d\n",tot_pedaco);

		int ult =0;
		int pedaco;
		int rank_id;

		#pragma omp parallel private(pedaco,rank_id) firstprivate(tot_pedaco,ult,sizeProc,cols,rows,nRows) shared(aval)
		{
			#pragma omp sections nowait
			{
			//#pragma omp for
			#pragma omp section
			{
				for(pedaco = 0; pedaco < tot_pedaco; pedaco++){//thread responsavel por enviar dados
					while( (rank_id = getProc(aval, &ult,sizeProc,-2)) == -1);
					
					//sinalizar que ha um trabalho a ser realizado
					MPI_Send( &pedaco, 1, MPI_INT, rank_id, 1, MPI_COMM_WORLD);

					//enviar matriz
					int curLine = (pedaco) * nRows;

 					if(pedaco == 0){
						//printf("mandei da linha %d ateh %d\n",curLine,curLine+(nRows+RANGE)-1);
						MPI_Send(&(image.data[curLine*sizeLine]), sizeLine * (nRows+RANGE), 
							MPI_UNSIGNED_CHAR, rank_id, 2, MPI_COMM_WORLD);

					}else if( pedaco == tot_pedaco -1 ){

                                		if( rows%nRows == 0 )//SEND_SIZE % LINE_SEND == 0)
 							MPI_Send(&(image.data[(curLine-RANGE)*sizeLine]), (sizeLine) * (nRows+RANGE), 
								MPI_UNSIGNED_CHAR, rank_id, 2, MPI_COMM_WORLD);
                                		else
							MPI_Send(&(image.data[(curLine-RANGE)*sizeLine]), (sizeLine) * ((rows%nRows)+RANGE), 
								MPI_UNSIGNED_CHAR, rank_id, 2, MPI_COMM_WORLD);
                        		}
                        		else{

						//printf("mandei da linha %d ateh %d\n",curLine-RANGE,curLine+nRows+RANGE-1);
						MPI_Send(&(image.data[(curLine-RANGE)*sizeLine]), (sizeLine) * (nRows+RANGE+RANGE), 
							MPI_UNSIGNED_CHAR, rank_id, 2, MPI_COMM_WORLD);
					}

					printf("[mestre] mandei o pedaco %d, para o %d\n",pedaco,rank_id);
				
					#pragma omp critical
					aval[rank_id] = pedaco;
					//curLine += nRows;
					//printf("curre = %d\n",curLine);
				}
			}

			
			//#pragma omp for 
			#pragma omp section
			{
				int ult =0;
				for(pedaco = 0; pedaco < tot_pedaco; pedaco++){//thread responsavel por receber resultados
					
					while( (rank_id = getProc(aval, &ult,sizeProc,1)) == -1);
					
					int pedaco_aux = aval[rank_id];
					int curLine = pedaco_aux * nRows;
					//enviar matriz
					printf("curre = %d\n",curLine);
					//MPI_Recv( &pedaco_aux, 1, MPI_INT, rank_id, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
					 printf("[mestre] recebi o pedaco %d, do %d\n",aval[rank_id],rank_id);
				
					if(pedaco_aux == 0){

						MPI_Recv(&(smoothed.data[curLine*sizeLine]), sizeLine * nRows, 
							MPI_UNSIGNED_CHAR, rank_id, 3, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

					}else if( pedaco_aux == tot_pedaco -1 ){

                                		if( rows%nRows == 0 )//SEND_SIZE % LINE_SEND == 0)
 							MPI_Recv(&(smoothed.data[curLine*sizeLine]), sizeLine * nRows, 
								MPI_UNSIGNED_CHAR, rank_id, 3, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                                		else
							MPI_Recv(&(smoothed.data[curLine*sizeLine]), sizeLine * (rows%nRows), 
								MPI_UNSIGNED_CHAR, rank_id, 3, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                        		}
                        		else{

						MPI_Recv(&(smoothed.data[curLine*sizeLine]), sizeLine * nRows, 
							MPI_UNSIGNED_CHAR, rank_id, 3, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
					}

					//MPI_Recv(&(image2.data[aval[rank_id]*cols*3]), cols*3, MPI_UNSIGNED_CHAR, 
					//	rank_id, 3, MPI_COMM_WORLD, MPI_STATUS_IGNORE);


					#pragma omp critical
					aval[rank_id] = -2;
                        		
					
				}
		

			}

			}


		}

		int job = -1;
		//finalizar comunicacao
		for(int i=1; i<sizeProc; i++ ){
			MPI_Send( &job, 1, MPI_INT, i, 1, MPI_COMM_WORLD);
		}

		cv::imwrite("Scorpion_Smoothed.jpg", smoothed);
		printf("[Mestre] Trabalho finalizado %d\n",tot_pedaco);

	}else{ //escravo

		
		//printf("mem %d %d\n", rows, cols);		
		sizeLine = cols * 3;
               		//printf("%d %d \n",rows, cols);

		while(1==1){//enquanto tem trabalhos para ser realizado

			 cv::Mat buff1(nRows+(2*RANGE), cols, 16); //capitura tarefa 
		               cv::Mat buff2(nRows, cols, 16); //resultado da tarefa
		//verificar se tem trabalho
			MPI_Recv( &pedaco, 1, MPI_INT, 0,1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			if(pedaco == -1) break;
			printf("[ESCRAVO] pedaco %d\n",pedaco);	
	
			if(pedaco == 0){
				MPI_Recv(&(buff1.data[0]), sizeLine * (nRows+RANGE), MPI_UNSIGNED_CHAR, MASTER, 2, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
				
				processamento(buff1, buff2, nRows, cols, 0,0);
			
				MPI_Send(&(buff2.data[0]), sizeLine*nRows, MPI_UNSIGNED_CHAR, MASTER, 3, MPI_COMM_WORLD);
		

			}else if( pedaco == tot_pedaco -1 ){

				if( rows%nRows == 0 ){

					MPI_Recv(&(buff1.data[0]), sizeLine * (nRows+RANGE), 
						MPI_UNSIGNED_CHAR, MASTER, 2, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
				
					processamento(buff1, buff2,nRows, cols, RANGE,1);
			
					MPI_Send(&(buff2.data[0]), sizeLine*nRows, MPI_UNSIGNED_CHAR, MASTER, 3, MPI_COMM_WORLD);

				}
				else{
					//printf("aqui\n");	
					MPI_Recv(&(buff1.data[0]), sizeLine * ((rows%nRows)+RANGE), 
						MPI_UNSIGNED_CHAR, MASTER, 2, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
				
					//processamento()
					processamento(buff1, buff2,rows%nRows, cols, RANGE,1);
			
					MPI_Send(&(buff2.data[0]), sizeLine*(rows%nRows), MPI_UNSIGNED_CHAR, MASTER, 3, MPI_COMM_WORLD);

				}
			}
			else{
				
				MPI_Recv(&(buff1.data[0]), sizeLine * (nRows+RANGE+RANGE), 
					MPI_UNSIGNED_CHAR, MASTER, 2, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
				
				//processamento()
				processamento(buff1, buff2, nRows, cols, RANGE,0);
			
				MPI_Send(&(buff2.data[0]), sizeLine*nRows, MPI_UNSIGNED_CHAR, MASTER, 3, MPI_COMM_WORLD);

			}
			//pegar dados para realizar computação.
			//MPI_Recv( &pedaco, 1, MPI_INT, 0,1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			//printf("ENVIADO\n");

		}

		printf("[Processo %d] Finalizado\n",rank);


	}


	MPI_Finalize();
	return 0;
}


	Existem dois Makefiles distintos neste projeto, um para a versão sequencial e
outro para a versão paralela. 
	Os Makefiles são constituídos das seguintes targets:

	>> compile: compila o código presente no diretório 'code' referente a versão do Makefile que está sendo executado.
	>> run: executa o cóodigo fim de testar se está tudo ok com o OpenMP e MPI.
	>> clean: limpa todos os arquivos gerados através do compile e run.
	>> gsp: dá como entrada e saída os nomes da imagem pequena em escala de cinza.
	>> gsm: dá como entrada e saída os nomes da imagem média em escala de cinza.
	>> gsg: dá como entrada e saída os nomes da imagem grande em escala de cinza.
	>> gse: dá como entrada e saída os nomes da imagem enorme em escala de cinza.
	>> rgbp: dá como entrada e saída os nomes da imagem pequena em RGB.
	>> rgbm: dá como entrada e saída os nomes da imagem m ́edia em RGB.
	>> rgbg: dá como entrada e saída os nomes da imagem grande em RGB.
	>> rgbe: dá como entrada e saída os nomes da imagem enorme em RGB.
	>> test: executa todos os oitos targets respectivos as imagens dos targets anteriores.

	As imagens de saída ficam na pasta bin.
	Diferentemente do Makefile sequencial, a versão paralela possui uma váriavel extra
que é a que recebe o argumento -np da execução do MPI. Desta forma o usuário
deve especificar através da chamada do Makefile qual será este argumento.
	$make core=16 tests

	De outra maneira, pode-se executar entrando na referida pasta (sequencial ou paralelo) e executando o make especifico.
	fazer: make run <nome_img_entrada.jpg> <nome_img_saida.jpg> tipo_img
	
	Os tipos das imagens podem ser : gray, rgb.

	Na pasta 'relatorio_saidas_script' contém o makefile (do programa paralelo), o spript, resultados e pdf com link das imagens que utilizamos para os testes no cluster Cosmos.  



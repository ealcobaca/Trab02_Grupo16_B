tests:
	@cd sequencial && $(MAKE) compile
	@cd ..
	@cd sequencial && $(MAKE) test
	@cd ..
	@cd paralelo && $(MAKE) compile
	@cd ..
	@cd paralelo && $(MAKE) test
	@cd ..
